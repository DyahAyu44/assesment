package xa.batch331.frontend_perpustakaan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontendPerpustakaanApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontendPerpustakaanApplication.class, args);
	}

}
