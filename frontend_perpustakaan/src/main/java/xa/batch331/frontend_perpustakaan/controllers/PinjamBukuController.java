package xa.batch331.frontend_perpustakaan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("pinjambuku")
public class PinjamBukuController {

    @GetMapping
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("pinjambuku/index");
        return view;
    }
}
