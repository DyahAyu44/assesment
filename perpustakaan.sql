--
-- PostgreSQL database dump
--

-- Dumped from database version 16.0
-- Dumped by pg_dump version 16.0

-- Started on 2024-01-26 10:53:34

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 216 (class 1259 OID 245752)
-- Name: buku; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.buku (
    id bigint NOT NULL,
    category_id bigint,
    judul character varying(255),
    penerbit character varying(255),
    tahun_terbit character varying(255)
);


ALTER TABLE public.buku OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 245751)
-- Name: buku_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.buku_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.buku_id_seq OWNER TO postgres;

--
-- TOC entry 4875 (class 0 OID 0)
-- Dependencies: 215
-- Name: buku_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.buku_id_seq OWNED BY public.buku.id;


--
-- TOC entry 218 (class 1259 OID 245761)
-- Name: category_buku; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category_buku (
    id bigint NOT NULL,
    category character varying(255),
    deskripsi character varying(255)
);


ALTER TABLE public.category_buku OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 245760)
-- Name: category_buku_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_buku_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.category_buku_id_seq OWNER TO postgres;

--
-- TOC entry 4876 (class 0 OID 0)
-- Dependencies: 217
-- Name: category_buku_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_buku_id_seq OWNED BY public.category_buku.id;


--
-- TOC entry 220 (class 1259 OID 245770)
-- Name: customers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customers (
    id bigint NOT NULL,
    alamat character varying(255),
    jk character varying(255),
    nama character varying(255)
);


ALTER TABLE public.customers OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 245769)
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.customers_id_seq OWNER TO postgres;

--
-- TOC entry 4877 (class 0 OID 0)
-- Dependencies: 219
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;


--
-- TOC entry 222 (class 1259 OID 245779)
-- Name: pinjam_buku; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pinjam_buku (
    id bigint NOT NULL,
    buku_id bigint,
    customer_id bigint,
    tgl_kembali character varying(255),
    tgl_pinjam character varying(255)
);


ALTER TABLE public.pinjam_buku OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 245778)
-- Name: pinjam_buku_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pinjam_buku_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.pinjam_buku_id_seq OWNER TO postgres;

--
-- TOC entry 4878 (class 0 OID 0)
-- Dependencies: 221
-- Name: pinjam_buku_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pinjam_buku_id_seq OWNED BY public.pinjam_buku.id;


--
-- TOC entry 4704 (class 2604 OID 245755)
-- Name: buku id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.buku ALTER COLUMN id SET DEFAULT nextval('public.buku_id_seq'::regclass);


--
-- TOC entry 4705 (class 2604 OID 245764)
-- Name: category_buku id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_buku ALTER COLUMN id SET DEFAULT nextval('public.category_buku_id_seq'::regclass);


--
-- TOC entry 4706 (class 2604 OID 245773)
-- Name: customers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);


--
-- TOC entry 4707 (class 2604 OID 245782)
-- Name: pinjam_buku id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_buku ALTER COLUMN id SET DEFAULT nextval('public.pinjam_buku_id_seq'::regclass);


--
-- TOC entry 4863 (class 0 OID 245752)
-- Dependencies: 216
-- Data for Name: buku; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.buku (id, category_id, judul, penerbit, tahun_terbit) FROM stdin;
1	2	Kancil Mencuri Timun	Dyah Ayu	2023
2	1	Dilan 1990	Pidi Baiq	2016
\.


--
-- TOC entry 4865 (class 0 OID 245761)
-- Dependencies: 218
-- Data for Name: category_buku; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category_buku (id, category, deskripsi) FROM stdin;
1	Novel	desc
2	Buku Cerita Anak	Anak
4	Buku Pelajaran	SMP
\.


--
-- TOC entry 4867 (class 0 OID 245770)
-- Dependencies: 220
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customers (id, alamat, jk, nama) FROM stdin;
1	Jakarta Barat	Perempuan	Salma Salsabil
2	Jakarta Selatan	Laki-laki	Rony Parulian
\.


--
-- TOC entry 4869 (class 0 OID 245779)
-- Dependencies: 222
-- Data for Name: pinjam_buku; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pinjam_buku (id, buku_id, customer_id, tgl_kembali, tgl_pinjam) FROM stdin;
1	2	1	15-01-2024	10-01-2024
\.


--
-- TOC entry 4879 (class 0 OID 0)
-- Dependencies: 215
-- Name: buku_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.buku_id_seq', 3, true);


--
-- TOC entry 4880 (class 0 OID 0)
-- Dependencies: 217
-- Name: category_buku_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_buku_id_seq', 4, true);


--
-- TOC entry 4881 (class 0 OID 0)
-- Dependencies: 219
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customers_id_seq', 4, true);


--
-- TOC entry 4882 (class 0 OID 0)
-- Dependencies: 221
-- Name: pinjam_buku_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pinjam_buku_id_seq', 2, true);


--
-- TOC entry 4709 (class 2606 OID 245759)
-- Name: buku buku_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.buku
    ADD CONSTRAINT buku_pkey PRIMARY KEY (id);


--
-- TOC entry 4711 (class 2606 OID 245768)
-- Name: category_buku category_buku_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category_buku
    ADD CONSTRAINT category_buku_pkey PRIMARY KEY (id);


--
-- TOC entry 4713 (class 2606 OID 245777)
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- TOC entry 4715 (class 2606 OID 245786)
-- Name: pinjam_buku pinjam_buku_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_buku
    ADD CONSTRAINT pinjam_buku_pkey PRIMARY KEY (id);


--
-- TOC entry 4717 (class 2606 OID 245797)
-- Name: pinjam_buku fk589utxhuaw9s1neg7i3hvl2ch; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_buku
    ADD CONSTRAINT fk589utxhuaw9s1neg7i3hvl2ch FOREIGN KEY (customer_id) REFERENCES public.customers(id);


--
-- TOC entry 4716 (class 2606 OID 245787)
-- Name: buku fkc4ftlrms5sgy59y97o6ric8pl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.buku
    ADD CONSTRAINT fkc4ftlrms5sgy59y97o6ric8pl FOREIGN KEY (category_id) REFERENCES public.category_buku(id);


--
-- TOC entry 4718 (class 2606 OID 245792)
-- Name: pinjam_buku fkir47n6mw4acc1xo5gqbpn1a2d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pinjam_buku
    ADD CONSTRAINT fkir47n6mw4acc1xo5gqbpn1a2d FOREIGN KEY (buku_id) REFERENCES public.buku(id);


-- Completed on 2024-01-26 10:53:34

--
-- PostgreSQL database dump complete
--

