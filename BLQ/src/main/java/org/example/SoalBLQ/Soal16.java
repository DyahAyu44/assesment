package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal16 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("* Asumsi harga pertama adalah makanan mengandung ikan");;
        System.out.println("Contoh input : 42000, 50000, 30000, 70000, 30000");
        System.out.println("Masukkan Harga : ");
        String harga = input.nextLine();

        String[] dataString = harga.split(", ");
        double[] dataArray = new double[dataString.length];
        double nonIkan = 0;
        double ikan = 0;

        for (int i = 0; i < dataString.length; i++) {
            dataArray[i] = Double.parseDouble(dataString[i]);
            dataArray[i] = dataArray[i] * 115 / 100;
        }

        for (int i = 0; i < dataArray.length; i++) {
            if (i == 0)
            {
                ikan = ikan + (dataArray[i] / 3);
            }
            else
            {
                ikan = ikan + (dataArray[i] / 4);
                nonIkan = nonIkan + (dataArray[i] / 4);
            }
        }

        System.out.println("Harga yang tidak memakan ikan = Rp." + nonIkan);
        System.out.println("Harga yang memakan ikan = Rp." + ikan);
    }
}
