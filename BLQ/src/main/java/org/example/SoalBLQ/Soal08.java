package org.example.SoalBLQ;

import java.util.Arrays;
import java.util.Scanner;

public class Soal08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Input Deret Angka : ");
        String deret = input.nextLine();

        String[] textArray = deret.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }

        int length = intArray.length;
        int min = 0;
        int max = 0;

        Arrays.sort(intArray);
        for (int i = 0; i < length-1; i++) {
            min = min + intArray[i];
        }
        for (int i = 1; i < length; i++) {
            max = max + intArray[i];
        }

        System.out.print("Terbesar : " + max);
        System.out.println();
        System.out.println("Terkecil : " + min);
        System.out.println();
    }
}
