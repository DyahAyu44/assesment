package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal18 {

    public static void Resolve(){

        System.out.println("Jam makan dan kalori dari kue yang dimakan Donna ");
        System.out.println("  JAM   |   KALORI   ");
        System.out.println("   9    |     30     ");
        System.out.println("   13   |     20     ");
        System.out.println("   15   |     50     ");
        System.out.println("   17   |     80     ");
        System.out.println("Donna mulai olahraga jam 18");

        int jamOlahraga = 18, selisihjam, air;
        double lamaOlahraga = 0;
        int[] jamMakan = {9, 13, 15, 17};
        int[] kalori = {30, 20, 50, 80};

        for (int i = 0; i < jamMakan.length; i++) {
            selisihjam = jamOlahraga - jamMakan[i];
            lamaOlahraga = lamaOlahraga + (0.1 * kalori[i] * selisihjam);
            jamOlahraga = 18;
        }

        lamaOlahraga = lamaOlahraga * 2;

        air = (int) (lamaOlahraga * 100) + 500;
        System.out.println("Air yang akan diminum Donna sepanjang berolahraga " + air + " cc air");
    }

}
