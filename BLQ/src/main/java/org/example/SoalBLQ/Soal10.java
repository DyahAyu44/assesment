package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Kalimat : ");
        String kalimat = input.nextLine();

        String[] splits = kalimat.split(" ");

        for (int i = 0; i < splits.length; i++) {
            char[] kalimatArray = splits[i].toCharArray();
            for (int j = 0; j < kalimatArray.length; j++) {
                if (j == 0){
                    System.out.print(kalimatArray[j] + "***");
                }
                else if (j == kalimatArray.length - 1){
                    System.out.print(kalimatArray[j] + " ");
                }
            }
        }
        System.out.println();
    }
}
