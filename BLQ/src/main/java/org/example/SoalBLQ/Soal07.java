package org.example.SoalBLQ;

import java.util.Arrays;
import java.util.Scanner;

public class Soal07 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Menentukan Mean, Median, Modus");
        System.out.print("Masukkan Deret Angka : ");
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        int[] angkaArray = new int[angkaSplit.length];

        for (int i = 0; i < angkaSplit.length; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }

        Arrays.sort(angkaArray);

        //Mean
        double mean = 0;
        for (int i = 0; i < angkaArray.length; i++) {
            mean += angkaArray[i];
        }

        double rataRata = mean / angkaArray.length;

        //Median
        double median = 0;
        if (angkaArray.length % 2 != 0) {
            median = angkaArray[angkaArray.length / 2];
        } else {
            median = (angkaArray[(angkaArray.length - 1) / 2] + (double) angkaArray[(angkaArray.length) / 2]) / 2;
        }

        //Modus
        int temp = 0;
        for (int i = 0; i < angkaArray.length; i++) {
            for (int j = i+1; j < angkaArray.length; j++) {
                if (angkaArray[i] < angkaArray[j])
                {
                    temp = angkaArray[i];
                    angkaArray[i] = angkaArray[j];
                    angkaArray[j] = temp;
                }
            }
        }
        double jumlahAngka = 0, jumlahModus = 0, modus = 0;
        for (int i = 0; i < angkaArray.length; i++) {
            for (int j = 0; j < angkaArray.length; j++) {
                if (angkaArray[i] == angkaArray[j] && i != j) {
                    jumlahAngka ++;
                }
            }
            if (jumlahAngka >= jumlahModus) {
                jumlahModus = jumlahAngka;
                modus = angkaArray[i];
                jumlahAngka = 0;
            }
        }

        System.out.println("Mean : " + rataRata);
        System.out.println("Median : " + median);
        System.out.println("Modus : " + modus);
    }
}
