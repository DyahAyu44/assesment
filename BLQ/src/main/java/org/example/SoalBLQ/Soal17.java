package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal17 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Contoh Input : N N T N N N T T T T T N T T T N T N");
        System.out.print("Masukkan Perjalanan Ninja Hattori : ");
        String ninja = input.nextLine().toUpperCase();

        String[] dataArray = ninja.split(" ");
        int helper = 0;
        int gunung = 0;
        int lembah = 0;

        for (int i = 0; i < dataArray.length; i++) {
            if (dataArray[i].equals("N"))
            {
                helper += 1;
            }
            else if (dataArray[i].equals("T"))
            {
                helper -= 1;
            }
            if (dataArray[i].equals("T") && helper == 0)
            {
                gunung++;
            } else if (dataArray[i].equals("N") && helper == 0)
            {
                lembah++;
            }

        }
        System.out.println("Jumlah Gunung = " + gunung);
        System.out.println("Jumlah Lembah = " + lembah);
    }
}
