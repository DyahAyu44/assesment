package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal19 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int isPangrams = 0;
        String sentence = "";

        System.out.print("Input Kalimat: ");
        String text = input.nextLine();

        String[] splits = text.split(" ");

        for (String str : splits) {
            sentence = sentence.concat(str);
        }

        char[] chars = sentence.toCharArray();

        for (int i = 'a'; i <= 'z' + 1; i++) {
            for (int j = 0; j < chars.length; j++) {
                if (chars[j] == i){
                    isPangrams++;
                    break;
                }
            }
        }

        if (isPangrams == 26){
            System.out.println("Pangram");
        }
        else {
            System.out.println("Not Pangram");
        }
    }
}
