package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nilai n : ");
        int n = input.nextInt();

        int helper = n;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = helper;
            helper += n;
        }

        for (int i = 0; i < hasil.length; i++) {
            System.out.print(hasil[i] + " ");

        }
        System.out.println("");
    }
}
