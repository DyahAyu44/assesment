package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan n : ");
        int n = input.nextInt();

        int helper = 1;
        int before = 0;
        int after = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = helper;
            helper = before + after;
            before = after;
            after = helper;

        }

        for (int i = 0; i < hasil.length; i++) {
            System.out.print(hasil[i] + " ");
        }
        System.out.println("");
    }
}
