package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan n : ");
        int n = input.nextInt();

        int helper = 1;
        boolean flag = true;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            while (flag){
                if (helper == 1){
                    helper++;
                }
                else if (helper == 2) {
                    results[i] = helper;
                    helper++;
                    flag = false;
                }
                else if (helper == 3) {
                    results[i] = helper;
                    helper++;
                    flag = false;
                }
                else if (helper % 2 != 0 && helper % 3 != 0) {
                    results[i] = helper;
                    helper++;
                    flag = false;
                }
                else {
                    helper++;
                }
            }
            flag = true;
        }

        for (int i = 0; i < results.length; i++) {
            System.out.print(results[i] + " ");

        }
        System.out.println("");
    }
}
