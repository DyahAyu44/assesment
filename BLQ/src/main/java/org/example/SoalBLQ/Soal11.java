package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Kata : ");
        String kata = input.nextLine();

        StringBuilder textReverse = new StringBuilder();
        textReverse.append(kata);
        textReverse.reverse();
        String textReverseString = textReverse.toString();
        char[] textReverseCharArray = textReverseString.toCharArray();
        if (textReverseCharArray.length % 2 == 0) {
            for (int i = 0; i < textReverseCharArray.length; i++) {
                System.out.println("***" + textReverseCharArray[i] + "***");
            }
        } else {
            for (int i = 0; i < textReverseCharArray.length; i++) {
                System.out.println("**" + textReverseCharArray[i] + "**");
            }
        }
        System.out.println();
    }
}
