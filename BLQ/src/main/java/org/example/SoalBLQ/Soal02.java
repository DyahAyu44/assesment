package org.example.SoalBLQ;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal02 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Kalkulasi Denda Buku");
        System.out.println(" Buku | Durasi (hari) ");
        System.out.println("   A  |     14     ");
        System.out.println("   B  |      3     ");
        System.out.println("   C  |      7     ");
        System.out.println("   D  |      7     ");

        // Data statis
        DateFormat df = new SimpleDateFormat("dd MMMM yyyy", new Locale("id"));
        String[][] bukuPeminjam = {{"A","B","C","D"},{"14","3","7","7"}};

        // Olah input
        System.out.println("Contoh Format input : 28 Februari 2016 - 7 Maret 2016");
        System.out.print("Masukkan tanggal peminjaman sampai pengembalian : ");
        String inputTanggal = input.nextLine();

        Date tanggalPinjam = null;
        Date tanggalPengembalian = null;
        boolean flag = true;

        while (flag){
            String[] splitTanggal = inputTanggal.split(" - ");
            try {
                tanggalPinjam = df.parse(splitTanggal[0]);
                tanggalPengembalian = df.parse(splitTanggal[1]);
                flag = false;
            } catch (ParseException e) {
                System.out.println("Format tidak sesuai!");
                System.out.println("Masukkan lagi tanggal peminjaman sampai pengembalian");
                inputTanggal = input.nextLine();
            }
        }

        long selisih = (tanggalPengembalian.getTime() - tanggalPinjam.getTime());
        int hari = (int) ((selisih / 1000) / 60 / 60 / 24);

        System.out.println("Output");
        for (int i = 0; i < bukuPeminjam[0].length; i++) {
            String namaBuku = bukuPeminjam[0][i];
            int durasiPeminjaman = Integer.parseInt(bukuPeminjam[1][i]);
            if (hari > durasiPeminjaman){
                int denda = (hari - durasiPeminjaman) * 100;
                System.out.print("Buku " + namaBuku + " Denda = " + denda);
            }else {
                System.out.print("Buku " + namaBuku + " Denda = " + 0);
            }
            System.out.println();
        }
    }

}
