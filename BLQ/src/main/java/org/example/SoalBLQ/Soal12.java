package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal12 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Deret Angka : ");
        String angka = input.nextLine();

        String[] angkaSplit = angka.split(" ");
        int[] angkaArray = new int[angkaSplit.length];

        for (int i = 0; i < angkaSplit.length; i++) {
            angkaArray[i] = Integer.parseInt(angkaSplit[i]);
        }

        int temp = 0;
        for (int i = 0; i < angkaArray.length; i++) {
            for (int j = i+1; j < angkaArray.length; j++) {
                if (angkaArray[i] > angkaArray[j]) {
                    temp = angkaArray[i];
                    angkaArray[i] = angkaArray[j];
                    angkaArray[j] = temp;
                }
            }
            System.out.print(angkaArray[i] + " ");
        }
        System.out.println();
    }
}
