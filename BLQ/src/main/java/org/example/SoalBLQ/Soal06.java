package org.example.SoalBLQ;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Menentukan kata/angka Palindrom atau Tidak");
        System.out.print("Masukkan kata/angka : ");
        String inputan = input.nextLine();

        char[] inputanChar = inputan.toCharArray();

        char[] inputanCharBalik = new char[inputanChar.length];

        int count = 1;
        for (int i = 0; i < inputanChar.length; i++) {
            inputanCharBalik[i] = inputanChar[inputanChar.length-count];
            count ++;
        }

        int helper = 0;
        for (int i = 0; i < inputanChar.length; i++) {
            if (inputanChar[i] == inputanCharBalik[i]) {
                helper ++;
            }
        }
        if (helper == inputanChar.length) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }
}
