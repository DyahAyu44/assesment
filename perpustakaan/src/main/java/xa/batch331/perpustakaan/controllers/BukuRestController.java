package xa.batch331.perpustakaan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan.models.Buku;
import xa.batch331.perpustakaan.models.CategoryBuku;
import xa.batch331.perpustakaan.services.BukuServices;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class BukuRestController {

    @Autowired
    private BukuServices bukuServices;

    @GetMapping("buku")
    public ResponseEntity<List<Buku>> getAllBuku() {
        try {
            List<Buku> buku = this.bukuServices.getAllBuku();
            return new  ResponseEntity<List<Buku>>(buku, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("buku/{id}")
    public ResponseEntity<?> getBukuById(@PathVariable("id") Long id) {
        try {
            Buku buku = this.bukuServices.getBukuById(id);
            return new  ResponseEntity<>(buku,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("buku")
    public ResponseEntity<Buku> saveBuku(@RequestBody Buku buku) {
        try {
            this.bukuServices.simpanBuku(buku);
            return new ResponseEntity<Buku>(buku, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("buku/{id}")
    public ResponseEntity<?> editBuku(@RequestBody Buku buku, @PathVariable("id") Long id) {
        try {
            buku.setId(id);
            this.bukuServices.simpanBuku(buku);
            Map<String, Object> result = new HashMap<>();
            String message = "Edit Data Berhasil !";
            result.put("Message", message);
            result.put("Data", buku);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("buku/{id}")
    public ResponseEntity<?> deleteBuku(@PathVariable("id") Long id) {
        try {
            Buku buku = this.bukuServices.getBukuById(id);
            if (buku != null) {
                this.bukuServices.deleteBuku(id);
                return ResponseEntity.status(HttpStatus.OK).body("Buku Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Buku Not Found");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchbuku={keyword}")
    public ResponseEntity<List<Buku>> searchBuku(@PathVariable("keyword") String keyword)
    {
        try {
            List<Buku> buku = this.bukuServices.searchBuku(keyword);
            return new ResponseEntity<>(buku, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
