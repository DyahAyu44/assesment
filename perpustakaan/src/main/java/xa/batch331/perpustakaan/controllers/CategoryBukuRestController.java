package xa.batch331.perpustakaan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan.models.CategoryBuku;;
import xa.batch331.perpustakaan.services.CategoryBukuService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CategoryBukuRestController {

    @Autowired
    private CategoryBukuService categoryBukuService;

    @GetMapping("categorybuku")
    public ResponseEntity<List<CategoryBuku>> getAllCategoryBuku() {
        try {
            List<CategoryBuku> categoryBuku = this.categoryBukuService.getAllCategoryBuku();
            return new  ResponseEntity<List<CategoryBuku>>(categoryBuku, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("categorybuku/{id}")
    public ResponseEntity<?> getCategoryBukuById(@PathVariable("id") Long id) {
        try {
            CategoryBuku categoryBuku = this.categoryBukuService.getCategoryBukuById(id);
            return new  ResponseEntity<>(categoryBuku,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("categorybuku")
    public ResponseEntity<CategoryBuku> saveCategoryBuku(@RequestBody CategoryBuku categoryBuku) {
        try {
            this.categoryBukuService.simpanCategoryBuku(categoryBuku);
            return new ResponseEntity<CategoryBuku>(categoryBuku, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("categorybuku/{id}")
    public ResponseEntity<?> editCategoryBuku(@RequestBody CategoryBuku categoryBuku, @PathVariable("id") Long id) {
        try {
            categoryBuku.setId(id);
            this.categoryBukuService.simpanCategoryBuku(categoryBuku);
            Map<String, Object> result = new HashMap<>();
            String message = "Edit Data Berhasil !";
            result.put("Message", message);
            result.put("Data", categoryBuku);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("categorybuku/{id}")
    public ResponseEntity<?> deleteCategoryBuku(@PathVariable("id") Long id) {
        try {
            CategoryBuku categoryBuku = this.categoryBukuService.getCategoryBukuById(id);
            if (categoryBuku != null) {
                this.categoryBukuService.deleteCategoryBuku(id);
                return ResponseEntity.status(HttpStatus.OK).body("Category Buku Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Category Buku Not Found");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchcategorybuku={keyword}")
    public ResponseEntity<List<CategoryBuku>> searchCategoryBuku(@PathVariable("keyword") String keyword)
    {
        try {
            List<CategoryBuku> catBuku = this.categoryBukuService.searchCategoryBuku(keyword);
            return new ResponseEntity<>(catBuku, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
