package xa.batch331.perpustakaan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan.models.PinjamBuku;
import xa.batch331.perpustakaan.services.PinjamBukuService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class PinjamBukuRestController {

    @Autowired
    private PinjamBukuService pinjamBukuService;

    @GetMapping("pinjambuku")
    public ResponseEntity<List<PinjamBuku>> getAllPinjamBuku() {
        try {
            List<PinjamBuku> pinjamBuku = this.pinjamBukuService.getAllPinjamBuku();
            return new  ResponseEntity<List<PinjamBuku>>(pinjamBuku, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("pinjambuku/{id}")
    public ResponseEntity<?> getPinjamBukuById(@PathVariable("id") Long id) {
        try {
            PinjamBuku pinjamBuku = this.pinjamBukuService.getPinjamBukuById(id);
            return new  ResponseEntity<>(pinjamBuku,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("pinjambuku")
    public ResponseEntity<PinjamBuku> savePinjamBuku(@RequestBody PinjamBuku pinjamBuku) {
        try {
            this.pinjamBukuService.simpanPinjamBuku(pinjamBuku);
            return new ResponseEntity<PinjamBuku>(pinjamBuku, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("pinjambuku/{id}")
    public ResponseEntity<?> editPinjamBuku(@RequestBody PinjamBuku pinjamBuku, @PathVariable("id") Long id) {
        try {
            pinjamBuku.setId(id);
            this.pinjamBukuService.simpanPinjamBuku(pinjamBuku);
            Map<String, Object> result = new HashMap<>();
            String message = "Edit Data Berhasil !";
            result.put("Message", message);
            result.put("Data", pinjamBuku);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("pinjambuku/{id}")
    public ResponseEntity<?> deletePinjamBuku(@PathVariable("id") Long id) {
        try {
            PinjamBuku pinjamBuku = this.pinjamBukuService.getPinjamBukuById(id);
            if (pinjamBuku != null) {
                this.pinjamBukuService.deletePinjamBuku(id);
                return ResponseEntity.status(HttpStatus.OK).body("Pinjam Buku Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Pinjam Buku Not Found");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
