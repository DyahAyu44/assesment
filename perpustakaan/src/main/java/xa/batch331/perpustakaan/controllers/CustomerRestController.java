package xa.batch331.perpustakaan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import xa.batch331.perpustakaan.models.CategoryBuku;
import xa.batch331.perpustakaan.models.Customer;
import xa.batch331.perpustakaan.services.CustomerService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class CustomerRestController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("customer")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            List<Customer> customer = this.customerService.getAllCustomer();
            return new  ResponseEntity<List<Customer>>(customer, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("customer/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") Long id) {
        try {
            Customer customer = this.customerService.getCustomerById(id);
            return new  ResponseEntity<>(customer,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("customer")
    public ResponseEntity<Customer> saveCustomer(@RequestBody Customer customer) {
        try {
            this.customerService.simpanCustomer(customer);
            return new ResponseEntity<Customer>(customer, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("customer/{id}")
    public ResponseEntity<?> editCustomer(@RequestBody Customer customer, @PathVariable("id") Long id) {
        try {
            customer.setId(id);
            this.customerService.simpanCustomer(customer);
            Map<String, Object> result = new HashMap<>();
            String message = "Edit Data Berhasil !";
            result.put("Message", message);
            result.put("Data", customer);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> result = new HashMap<>();
            result.put("Message", e.getMessage());
            result.put("Data", null);
            return new ResponseEntity(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("customer/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable("id") Long id) {
        try {
            Customer product = this.customerService.getCustomerById(id);
            if (product != null) {
                this.customerService.deleteCustomer(id);
                return ResponseEntity.status(HttpStatus.OK).body("Customer Deleted");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Customer Not Found");
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("searchcustomer={keyword}")
    public ResponseEntity<List<Customer>> searchCustomer(@PathVariable("keyword") String keyword)
    {
        try {
            List<Customer> customer = this.customerService.searchCustomer(keyword);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
