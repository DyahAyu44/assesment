package xa.batch331.perpustakaan.models;

import jakarta.persistence.*;

@Entity
@Table(name = "buku")
public class Buku {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    public CategoryBuku categoryBuku;

    @Column(name = "category_id")
    private Long CategoryId;

    @Column(name = "judul")
    private String Judul;

    @Column(name = "penerbit")
    private String Penerbit;

    @Column(name = "tahun_terbit")
    private String TahunTerbit;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public CategoryBuku getCategoryBuku() {
        return categoryBuku;
    }

    public void setCategoryBuku(CategoryBuku categoryBuku) {
        this.categoryBuku = categoryBuku;
    }

    public Long getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(Long categoryId) {
        CategoryId = categoryId;
    }

    public String getJudul() {
        return Judul;
    }

    public void setJudul(String judul) {
        Judul = judul;
    }

    public String getPenerbit() {
        return Penerbit;
    }

    public void setPenerbit(String penerbit) {
        Penerbit = penerbit;
    }

    public String getTahunTerbit() {
        return TahunTerbit;
    }

    public void setTahunTerbit(String tahunTerbit) {
        TahunTerbit = tahunTerbit;
    }
}
