package xa.batch331.perpustakaan.models;

import jakarta.persistence.*;

@Entity
@Table(name = "pinjam_buku")
public class PinjamBuku {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "customer_id", insertable = false, updatable = false)
    public Customer customer;

    @Column(name = "customer_id")
    private Long CustomerId;

    @ManyToOne
    @JoinColumn(name = "buku_id", insertable = false, updatable = false)
    public Buku buku;

    @Column(name = "buku_id")
    private Long BukuId;

    @Column(name = "tgl_pinjam")
    private String TglPinjam;

    @Column(name = "tgl_kembali")
    private String TglKembali;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(Long customerId) {
        CustomerId = customerId;
    }

    public Buku getBuku() {
        return buku;
    }

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public Long getBukuId() {
        return BukuId;
    }

    public void setBukuId(Long bukuId) {
        BukuId = bukuId;
    }

    public String getTglPinjam() {
        return TglPinjam;
    }

    public void setTglPinjam(String tglPinjam) {
        TglPinjam = tglPinjam;
    }

    public String getTglKembali() {
        return TglKembali;
    }

    public void setTglKembali(String tglKembali) {
        TglKembali = tglKembali;
    }
}
