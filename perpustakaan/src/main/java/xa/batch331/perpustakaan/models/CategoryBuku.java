package xa.batch331.perpustakaan.models;

import jakarta.persistence.*;

@Entity
@Table(name = "category_buku")
public class CategoryBuku {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "category")
    private String Category;

    @Column(name = "deskripsi")
    private String Deskripsi;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }
}
