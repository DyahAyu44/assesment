package xa.batch331.perpustakaan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan.models.Buku;

import java.util.List;

@Repository
public interface BukuRepo extends JpaRepository<Buku, Long> {

    @Query( value = "select * FROM buku WHERE lower(judul) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<Buku> SearchBuku(String keyword);
}
