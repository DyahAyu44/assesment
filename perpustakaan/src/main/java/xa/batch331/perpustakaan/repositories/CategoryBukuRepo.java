package xa.batch331.perpustakaan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan.models.CategoryBuku;

import java.util.List;

@Repository
public interface CategoryBukuRepo extends JpaRepository<CategoryBuku, Long> {

    @Query( value = "select * FROM category_buku WHERE lower(category) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<CategoryBuku> SearchCategoryBuku(String keyword);
}
