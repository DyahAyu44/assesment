package xa.batch331.perpustakaan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import xa.batch331.perpustakaan.models.Customer;

import java.util.List;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {

    @Query( value = "select * FROM customers WHERE lower(nama) LIKE lower(concat('%',:keyword,'%'))", nativeQuery = true)
    List<Customer> SearchCustomer(String keyword);
}
