package xa.batch331.perpustakaan.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan.models.Customer;
import xa.batch331.perpustakaan.repositories.CustomerRepo;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepo customerRepo;

    public List<Customer> getAllCustomer() {
        return this.customerRepo.findAll();
    }

    public void simpanCustomer(Customer customer) {
        this.customerRepo.save(customer);
    }

    public Customer getCustomerById(Long id) {
        return this.customerRepo.findById(id).orElse(null);
    }

    public void deleteCustomer(Long id) {
        this.customerRepo.deleteById(id);
    }

    public List<Customer> searchCustomer(String keyword) {
        return this.customerRepo.SearchCustomer(keyword);
    }
}
