package xa.batch331.perpustakaan.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan.models.CategoryBuku;
import xa.batch331.perpustakaan.repositories.CategoryBukuRepo;

import java.util.List;

@Service
public class CategoryBukuService {

    @Autowired
    private CategoryBukuRepo categoryBukuRepo;

    public List<CategoryBuku> getAllCategoryBuku() {
        return this.categoryBukuRepo.findAll();
    }

    public void simpanCategoryBuku(CategoryBuku categoryBuku) {
        this.categoryBukuRepo.save(categoryBuku);
    }

    public CategoryBuku getCategoryBukuById(Long id) {
        return this.categoryBukuRepo.findById(id).orElse(null);
    }

    public void deleteCategoryBuku(Long id) {
        this.categoryBukuRepo.deleteById(id);
    }

    public List<CategoryBuku> searchCategoryBuku(String keyword) {
        return this.categoryBukuRepo.SearchCategoryBuku(keyword);
    }
}
