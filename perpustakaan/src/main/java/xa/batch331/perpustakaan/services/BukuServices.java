package xa.batch331.perpustakaan.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan.models.Buku;
import xa.batch331.perpustakaan.repositories.BukuRepo;

import java.util.List;

@Service
public class BukuServices {

    @Autowired
    private BukuRepo bukuRepo;

    public List<Buku> getAllBuku() {
        return this.bukuRepo.findAll();
    }

    public void simpanBuku(Buku buku) {
        this.bukuRepo.save(buku);
    }

    public Buku getBukuById(Long id) {
        return this.bukuRepo.findById(id).orElse(null);
    }

    public void deleteBuku(Long id) {
        this.bukuRepo.deleteById(id);
    }

    public List<Buku> searchBuku(String keyword) {
        return this.bukuRepo.SearchBuku(keyword);
    }
}
