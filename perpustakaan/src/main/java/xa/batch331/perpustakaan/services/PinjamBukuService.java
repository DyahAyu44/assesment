package xa.batch331.perpustakaan.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xa.batch331.perpustakaan.models.PinjamBuku;
import xa.batch331.perpustakaan.repositories.PinjamBukuRepo;

import java.util.List;

@Service
public class PinjamBukuService {

    @Autowired
    private PinjamBukuRepo pinjamBukuRepo;

    public List<PinjamBuku> getAllPinjamBuku() {
        return this.pinjamBukuRepo.findAll();
    }

    public void simpanPinjamBuku(PinjamBuku pinjamBuku) {
        this.pinjamBukuRepo.save(pinjamBuku);
    }

    public PinjamBuku getPinjamBukuById(Long id) {
        return this.pinjamBukuRepo.findById(id).orElse(null);
    }

    public void deletePinjamBuku(Long id) {
        this.pinjamBukuRepo.deleteById(id);
    }
}
